import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-paso3',
  templateUrl: './paso3.page.html',
  styleUrls: ['./paso3.page.scss'],
})
export class Paso3Page implements OnInit {
  dataUser:any;
  cardNumber:string;
  cardName:string;
  cardMonth:string;
  cardYear:number;
  cardCVV:number;
  


  constructor(private route:ActivatedRoute ,private router: Router) {
    this.route.queryParams.subscribe(params => {
      if(this.router.getCurrentNavigation().extras.state){
        this.dataUser = this.router.getCurrentNavigation().extras.state.user;
        console.table(this.dataUser);
      }
    });
   }

  ngOnInit() {
  }
  continuar(){
    let card:CreditCard=new CreditCard(this.cardNumber,this.cardName, this.cardMonth,this.cardYear,this.cardCVV);

    let navigationExtras: NavigationExtras={
      state:{
        user: this.dataUser,
        card: card,
      }
    };
    this.router.navigate(['/paso4'],navigationExtras);
  }

}

class CreditCard{
  cardNumber:string;
  cardName:string;
  cardMonth:string;
  cardYear:number;
  cardCVV:number;
  constructor(cardNumber:string,
              cardName:string,
              cardMonth:string,
              cardYear:number,
              cardCVV:number){
                this.cardNumber=cardNumber;
                this.cardName=cardName;
                this.cardMonth=cardMonth;
                this.cardYear=cardYear;
                this.cardCVV=cardCVV;
              }
}
