import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';


@Component({
  selector: 'app-paso4',
  templateUrl: './paso4.page.html',
  styleUrls: ['./paso4.page.scss'],
})
export class Paso4Page implements OnInit {

  dataUser:any;
  card:any
  direccion:string;

  constructor(private route:ActivatedRoute ,private router: Router) {
    this.route.queryParams.subscribe(params => {
      if(this.router.getCurrentNavigation().extras.state){
        this.dataUser = this.router.getCurrentNavigation().extras.state.user;
        this.card = this.router.getCurrentNavigation().extras.state.card;
        console.table(this.dataUser);
        console.table(this.card);

        this.direccion=this.dataUser.userDireccionOne+"; "+this.dataUser.userCiudad+" - "+this.dataUser.userCiudad+"."
      }
    });
   }

  ngOnInit() {
  }

}



class User{
  userMail:string
  userName:string;
  userSurname:string;
  userEmpresa:string;
  userDireccionOne:string;
  userDireccionTwo:string;
  userPais:string;
  userCiudad:string;
  userProvincia:string;
  userCP:string;
  userTelf:string;
  constructor(userMail:string,
              userName:string,
              userSurname:string,
              userEmpresa:string,
              userDireccionOne:string,
              userDireccionTwo:string,
              userPais:string,
              userCiudad:string,
              userProvincia:string,
              userCP:string,
              userTelf:string)
    {
      this.userMail=userMail;
      this.userName=userName;
      this.userSurname=userSurname;
      this.userEmpresa=userEmpresa;
      this.userDireccionOne=userDireccionOne;
      this.userDireccionTwo=userDireccionTwo;
      this.userPais=userPais;
      this.userCiudad=userCiudad;
      this.userProvincia=userProvincia;
      this.userCP=userCP;
      this.userTelf=userTelf;
  }
  
}

class CreditCard{
  cardNumber:string;
  cardName:string;
  cardMonth:string;
  cardYear:number;
  cardCVV:number;
  constructor(cardNumber:string,
              cardName:string,
              cardMonth:string,
              cardYear:number,
              cardCVV:number){
                this.cardNumber=cardNumber;
                this.cardName=cardName;
                this.cardMonth=cardMonth;
                this.cardYear=cardYear;
                this.cardCVV=cardCVV;
              }
}
