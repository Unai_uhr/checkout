import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'paso1',
    loadChildren: () => import('./paso1/paso1.module').then( m => m.Paso1PageModule)
  },
  {
    path: 'paso2',
    loadChildren: () => import('./paso2/paso2.module').then( m => m.Paso2PageModule)
  },
  {
    path: 'paso3',
    loadChildren: () => import('./paso3/paso3.module').then( m => m.Paso3PageModule)
  },
  {
    path: 'paso4',
    loadChildren: () => import('./paso4/paso4.module').then( m => m.Paso4PageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
