import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-paso1',
  templateUrl: './paso1.page.html',
  styleUrls: ['./paso1.page.scss'],
})
export class Paso1Page implements OnInit {
  
  userMail:string
  userName:string;
  userSurname:string;
  userEmpresa:string;
  userDireccionOne:string;
  userDireccionTwo:string;
  userPais:string;
  userCiudad:string;
  userProvincia:string;
  userCP:string;
  userTelf:string;
  
  constructor(
    private router:Router
    ) {}

  ngOnInit() {
  }

  continuar(){

    let usuario:User= new User( this.userMail,this.userName,this.userSurname,this.userEmpresa,this.userDireccionOne,this.userDireccionTwo,this.userPais,this.userCiudad,this.userProvincia,this.userCP,this.userTelf
                                )
    let navigationExtras: NavigationExtras={
      state:{
        parametros: usuario,
      }
    };
    this.router.navigate(['/paso2'],navigationExtras);
  }

}


 class User{
  userMail:string
  userName:string;
  userSurname:string;
  userEmpresa:string;
  userDireccionOne:string;
  userDireccionTwo:string;
  userPais:string;
  userCiudad:string;
  userProvincia:string;
  userCP:string;
  userTelf:string;
  constructor(userMail:string,
              userName:string,
              userSurname:string,
              userEmpresa:string,
              userDireccionOne:string,
              userDireccionTwo:string,
              userPais:string,
              userCiudad:string,
              userProvincia:string,
              userCP:string,
              userTelf:string)
    {
      this.userMail=userMail;
      this.userName=userName;
      this.userSurname=userSurname;
      this.userEmpresa=userEmpresa;
      this.userDireccionOne=userDireccionOne;
      this.userDireccionTwo=userDireccionTwo;
      this.userPais=userPais;
      this.userCiudad=userCiudad;
      this.userProvincia=userProvincia;
      this.userCP=userCP;
      this.userTelf=userTelf;
  }
  
}


