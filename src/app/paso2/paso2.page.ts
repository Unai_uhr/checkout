import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router,NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-paso2',
  templateUrl: './paso2.page.html',
  styleUrls: ['./paso2.page.scss'],
})
export class Paso2Page implements OnInit {
  data :any;
  userName="";
  userSurname="";
  userDireccion="";
  userTelf="";

  constructor(private route:ActivatedRoute ,private router: Router) { 
    this.route.queryParams.subscribe(params => {
      if(this.router.getCurrentNavigation().extras.state){
        this.data = this.router.getCurrentNavigation().extras.state.parametros;
        //console.table(this.data)
        this.userName=this.data.userName;
        this.userSurname=this.data.userSurname;
        this.userDireccion=this.data.userProvincia+" , "+this.data.userCiudad+" , "+this.data.userCP+" , "+this.data.userPais
        this.userTelf=this.data.userTelf;
      }
    });
  }

  ngOnInit() {
  }


  continuar(){

    
    let navigationExtras: NavigationExtras={
      state:{
        user: this.data,
      }
    };
    this.router.navigate(['/paso3'],navigationExtras);
  }

}
